package ru.sber.jd.sber.exceptions;

import lombok.Data;

@Data
public class UnknownException extends Exception {

    private String message;

    private Integer httpStatus = 502;

    public UnknownException(String message) {
        this.message = message;
    }
}
