package ru.sber.jd.sber.utils.mapers.to_entity;

import org.springframework.stereotype.Service;
import ru.sber.jd.sber.dto.PersonAuthorizeDto;
import ru.sber.jd.sber.entities.PersonEntity;

@Service
public class MapToPersonEntity {

    public PersonEntity map(PersonAuthorizeDto dto) {
        PersonEntity entity = new PersonEntity()
                .setId(dto.getId())
                .setName(dto.getName())
                .setVariable(dto.getVariable())
                .setPassword(dto.getPassword())
                .setLogin(dto.getLogin())
                .setPersonUuid(dto.getPersonUuid());

        return entity;
    }
}
