package ru.sber.jd.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class PersonAuthorizeDto {

    private Integer id;

    private String variable;

    private String name;

    private UUID personUuid;

    private String login;

    private String password;

    private UUID tokenSession;
}
