package ru.sber.jd.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.sber.dto.PersonAuthorizeDto;
import ru.sber.jd.sber.exceptions.*;
import ru.sber.jd.sber.services.PersonService;
import ru.sber.jd.sber.utils.exception.ControllersException;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;
    private final ControllersException controllersException;
    private final AuthorisationController authorisationController;
//    private final CurrentPerson currentPerson;

    @PostMapping("/save/")
    public ResponseEntity<PersonAuthorizeDto> save(@RequestBody PersonAuthorizeDto dto) throws Exception {
        ResponseEntity<PersonAuthorizeDto> ok = ResponseEntity.ok(personService.save(dto));
        controllersException.throwException(ok);
        return ok;
    }

    @PostMapping("/authorization")
    public String authorization(@RequestBody PersonAuthorizeDto dto) throws Exception {

        PersonAuthorizeDto authorization = personService.authorization(dto);
        ResponseEntity<PersonAuthorizeDto> ok = ResponseEntity.ok(authorization);
        controllersException.throwException(ok);
        authorisationController.run(authorization);
        return "Пользователь найден " +
                Objects.requireNonNull(ok.getBody()).getTokenSession() +
                "\nПользовательская сессия открыта";
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<PersonAuthorizeDto> find(@PathVariable Integer id) throws Exception {
        ResponseEntity<PersonAuthorizeDto> ok = ResponseEntity.ok(personService.getById(id));
        if (personService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwException(ok);
        return ok;
    }

    @GetMapping("/findAll/")
    public ResponseEntity<List<PersonAuthorizeDto>> findAll() throws Exception {
        ResponseEntity<List<PersonAuthorizeDto>> ok = ResponseEntity.ok(personService.getAll());
        controllersException.throwException(ok);
        return ok;
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable Integer id) {
        personService.delete(id);
    }

    @PutMapping("/putById/{id}")
    public void putById(@PathVariable Integer id, @RequestBody PersonAuthorizeDto dto) {
        PersonAuthorizeDto byId = personService.getById(id);
        byId.setName(dto.getName());
        byId.setVariable(dto.getVariable());
        byId.setLogin(dto.getLogin());
        byId.setPassword(dto.getPassword());
        byId.setPersonUuid(dto.getPersonUuid());
        personService.save(byId);
    }

    @ExceptionHandler(NoFoundException.class)
    public static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    public ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    public ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    public ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
