package ru.sber.jd.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.sber.entities.PersonEntity;
import ru.sber.jd.sber.repositories.PersonRepository;

import java.util.List;


@Service
@RequiredArgsConstructor
public class FillerUtil {
    private final PersonRepository personRepository;


    public void addPerson(List<PersonEntity> person) {
        personRepository.saveAll(person);
    }

}
