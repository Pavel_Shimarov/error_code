package ru.sber.jd.sber.exceptions;

import lombok.Data;

@Data
public class IncorrectInputException extends Exception {

    private String message;

    private Integer httpStatus;

    public IncorrectInputException(String message) {
        this.message = "400 Bad Request («не верный " + message + " ввод»)";
    }
}
