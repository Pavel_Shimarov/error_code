package ru.sber.jd.sber.exceptions;

import lombok.Data;

@Data
public class InternalServerError extends Error {

    private String message;

    private Integer httpStatus;

    @Override
    public String toString() {
        return "InternalServerError{" +
                "message='" + message + '\'' +
                '}';
    }
}
