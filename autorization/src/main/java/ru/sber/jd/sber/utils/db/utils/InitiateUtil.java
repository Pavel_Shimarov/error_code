package ru.sber.jd.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.sber.entities.PersonEntity;
import ru.sber.jd.sber.repositories.PersonRepository;


@Service
@RequiredArgsConstructor
public class InitiateUtil implements CommandLineRunner {


    private final PersonRepository personRepository;
    private final VolumePersonTable volumePersonTable;


    @Override
    public void run(String... args) throws Exception {

        volumePersonTable.fillVolumeTechTable();


        System.out.println("\n   Таблица персон");
        for (PersonEntity person : personRepository.findAll()) {
            System.out.println(person);
        }
    }
}

