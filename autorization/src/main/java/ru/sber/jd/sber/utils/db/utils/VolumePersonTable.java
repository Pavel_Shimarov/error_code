package ru.sber.jd.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.sber.aspect.LoggableAnnotation;
import ru.sber.jd.sber.entities.PersonEntity;
import ru.sber.jd.sber.enums.PersonVariable;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class VolumePersonTable {

    private final FillerUtil fillerUtil;

    @LoggableAnnotation
    private List<PersonEntity> getListPerson() throws NoSuchAlgorithmException {
        List<PersonEntity> persons = new ArrayList<>(
                Arrays.asList(
                        new PersonEntity()
                                .setVariable(PersonVariable.USER.toString())
                                .setName("User1")
                                .setLogin("User1")
                                .setPassword(codingSha256("Password"))
                                .setPersonUuid(UUID.randomUUID()),
                        new PersonEntity()
                                .setVariable(PersonVariable.USER.toString())
                                .setName("User2")
                                .setLogin("User2")
                                .setPassword(codingSha256("Password"))
                                .setPersonUuid(UUID.randomUUID()),
                        new PersonEntity()
                                .setVariable(PersonVariable.ADMINISTRATOR.toString())
                                .setName("Admin1")
                                .setLogin("Admin1")
                                .setPassword(codingSha256("Password"))
                                .setPersonUuid(UUID.randomUUID()),
                        new PersonEntity()
                                .setVariable(PersonVariable.ADMINISTRATOR.toString())
                                .setName("Admin2")
                                .setLogin("Admin2")
                                .setPassword(codingSha256("Password"))
                                .setPersonUuid(UUID.randomUUID())
                )
        );
        return persons;
    }

    public void fillVolumeTechTable() throws NoSuchAlgorithmException {
        List<PersonEntity> listPerson = getListPerson();
        fillerUtil.addPerson(listPerson);
    }

    public static String codingSha256(String str) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(str.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        return String.format("%064x", new BigInteger(1, digest));
    }
}
