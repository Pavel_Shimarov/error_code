package ru.sber.jd.sber.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class KafkaException extends IllegalThreadStateException {

    private String message;

    public KafkaException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "KafkaException{" +
                "message='" + message + '\'' +
                '}';
    }
}
