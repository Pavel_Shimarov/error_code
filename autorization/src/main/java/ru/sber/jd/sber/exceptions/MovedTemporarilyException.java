package ru.sber.jd.sber.exceptions;

import lombok.Data;

@Data
public class MovedTemporarilyException extends Exception {

    private String message;

    private Integer httpStatus = 302;

    @Override
    public String toString() {
        return "MovedTemporarilyException{" +
                "message='" + message + '\'' +
                '}';
    }
}
