package ru.sber.jd.sber.entities;


import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Accessors(chain = true)
@Data
@Entity
@Table(name = "person_table")

public class PersonEntity {

    @Id
    @Column(name = "person_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer id;

    @Column(name = "person_variable")
    private String variable;

    @Column(name = "person_name")
    private String name;

    @Column(name = "person_UUID")
    private UUID personUuid;

    @Column(name = "person_login")
    private String login;

    @Column(name = "person_password")
    private String password;
}
