package ru.sber.jd.sber.utils.mapers.to_dto;

import org.springframework.stereotype.Service;
import ru.sber.jd.sber.dto.PersonAuthorizeDto;
import ru.sber.jd.sber.entities.PersonEntity;


@Service
public class MapToPersonDto {
    public PersonAuthorizeDto mapToDto(PersonEntity entity) {
        PersonAuthorizeDto dto = new PersonAuthorizeDto()
                .setId(entity.getId())
                .setName(entity.getName())
                .setLogin(entity.getLogin())
                .setVariable(entity.getVariable())
//                .setPassword(entity.getPassword())
                .setPersonUuid(entity.getPersonUuid());
        return dto;
    }
}
