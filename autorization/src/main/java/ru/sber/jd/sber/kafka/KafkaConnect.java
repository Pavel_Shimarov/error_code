package ru.sber.jd.sber.kafka;


import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.sber.jd.sber.dto.PersonAuthorizeDto;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Service
public class KafkaConnect {


    private final static String TOPIC = "jd_topic";
    private final static String BOOTSTRAP_SERVER = "localhost:9092";
    private Thread producerThread = null;
    private Thread consumerThread = null;
    private Long countProducerRecord = null;


    public void setProducerThread(ResponseEntity<PersonAuthorizeDto> person) {

        producerThread = new Thread(() -> {
            final Producer<String, String> producer = createProducer();
            while (true) {

                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                countProducerRecord++;
                final ProducerRecord<String, String> record =
                        new ProducerRecord<>(TOPIC, "Номер записи" + countProducerRecord, person.toString());

                try {
                    RecordMetadata metadata = producer.send(record).get();
                } catch (InterruptedException | ExecutionException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
        producerThread.start();
        producerThread.setDaemon(true);
    }

    public void setConsumerThread() {
        consumerThread = new Thread(() -> {
            final Consumer<String, String> consumer = createConsumer();
            while (true) {
                final ConsumerRecords<String, String> consumerRecord = consumer.poll(1000);

                if (consumerRecord.count() == 0) {
                    continue;
                }
                consumerRecord.forEach(record -> {
                    System.out.printf("Consumer Record: (%s, %s, %d, %d\n)", record.key(), record.value(), record.partition(), record.offset());
                });
                consumer.commitAsync();
            }
        });

        consumerThread.start();
        consumerThread.setDaemon(true);
    }

    private static Producer<String, String> createProducer() {
        Properties prop = new Properties();
        prop.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        prop.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        prop.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        prop.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(prop);
    }

    private static Consumer<String, String> createConsumer() {
        Properties prop = new Properties();
        prop.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        prop.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleProducer");
        prop.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        prop.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        final Consumer<String, String> consumer = new KafkaConsumer<>(prop);
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }

    public void thread() throws InterruptedException {
        KafkaConnect kafkaConnect = new KafkaConnect();
        kafkaConnect.consumerThread.join();
        kafkaConnect.producerThread.join();
    }
}
