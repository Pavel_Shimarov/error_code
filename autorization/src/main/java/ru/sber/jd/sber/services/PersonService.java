package ru.sber.jd.sber.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.sber.dto.PersonAuthorizeDto;
import ru.sber.jd.sber.entities.PersonEntity;
import ru.sber.jd.sber.repositories.PersonRepository;
import ru.sber.jd.sber.utils.db.utils.VolumePersonTable;
import ru.sber.jd.sber.utils.mapers.to_dto.MapToPersonDto;
import ru.sber.jd.sber.utils.mapers.to_entity.MapToPersonEntity;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;
    private final MapToPersonDto mapToPersonDto;
    private final MapToPersonEntity mapToPersonEntity;

    public PersonAuthorizeDto save(PersonAuthorizeDto dto) {
        PersonEntity entity = mapToPersonEntity.map(dto);
        PersonEntity saveEntity = personRepository.save(entity);
        return mapToPersonDto.mapToDto(saveEntity);
    }

    public PersonAuthorizeDto authorization(PersonAuthorizeDto dto) throws NoSuchAlgorithmException {
        PersonEntity byLoginAndPassword =
                personRepository.findByLoginAndPassword(dto.getLogin(), VolumePersonTable.codingSha256(dto.getPassword()));
        return mapToPersonDto.mapToDto(byLoginAndPassword);
    }

    public List<PersonAuthorizeDto> getAll() {
        return personRepository.findAll().stream()
                .map(mapToPersonDto::mapToDto)
                .collect(Collectors.toList());
    }

    public PersonAuthorizeDto getById(Integer id) {
        return mapToPersonDto.mapToDto(personRepository.findById(id)
                .orElse(new PersonEntity()));
    }

    public void delete(Integer id) {
        personRepository.deleteById(id);
    }
}
