package ru.sber.jd.sber.aspect;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;
import ru.sber.jd.sber.loggers.PrinterLogs;

import java.time.LocalDateTime;
import java.util.Arrays;

@Service
@Aspect
@RequiredArgsConstructor
public class LoggableAspect {

    private final PrinterLogs printerLogs;


    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping) " +
            "|| @annotation(org.springframework.web.bind.annotation.PostMapping) " +
            "|| @annotation(org.springframework.web.bind.annotation.PutMapping)")
    //можно залогировать таким образом все ресты
    public void loggablePointCut() {

    }

    @Pointcut("@annotation(LoggableAnnotation)")
    public void initiatePointCutLoggable() {

    }


    @Around("loggablePointCut()")
    public Object aroundLoggable(ProceedingJoinPoint pjp) throws Throwable {
        Object retVal = null;
        try {
            String logAfterRunMethod = "\n" + LocalDateTime.now() + ": Класс: "
                    + pjp.getThis().getClass().getName().split("\\$\\$")[0]
                    + "; Метод: " +
                    pjp.getSignature().toShortString() +
                    "; Входные параметры: " + Arrays.toString(pjp.getArgs());
            System.out.println(logAfterRunMethod);
            printerLogs.writeLogByFile(logAfterRunMethod);
            retVal = pjp.proceed();//вызов оригинального метода
            String logBeforeRunMethod = LocalDateTime.now() + ": Класс: " + pjp.getThis().getClass().getName().split("\\$\\$")[0]
                    + "; Метод: " +
                    pjp.getSignature().toShortString() +
                    "; Выходные параметры: " + retVal;
            System.out.println(logBeforeRunMethod);
            printerLogs.writeLogByFile(logBeforeRunMethod);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return retVal;
    }

    @Around("initiatePointCutLoggable()")
    public Object aroundInitiate(ProceedingJoinPoint pjp) throws Throwable {
        Object retVal = null;
        try {
            retVal = pjp.proceed();
            String logAfterRunMethod = "Класс: " + pjp.getThis().getClass().getName().split("\\$\\$")[0]
                    + "; Метод: " + pjp.getSignature().toShortString();
            System.out.println(logAfterRunMethod);
            printerLogs.writeLogByFile("\n" + logAfterRunMethod + retVal);
        } catch (Exception e) {
            e.getMessage();
        }
        return retVal;
    }
}
