package ru.sber.jd.sber.exceptions;

import lombok.Data;

@Data
public class NoFoundException extends Exception {

    private String message;
    private Integer httpStatus = 404;

}
