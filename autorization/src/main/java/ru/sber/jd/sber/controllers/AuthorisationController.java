package ru.sber.jd.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.sber.jd.sber.dto.PersonAuthorizeDto;
import ru.sber.jd.sber.kafka.KafkaConnect;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthorisationController {

    private final KafkaConnect kafkaConnect;

    private final RestTemplate restTemplate = new RestTemplate();

    public void run(PersonAuthorizeDto person) {
        try {
            kafkaConnect.setProducerThread(responseEntity(person));
        } catch (Exception e) {
            responseEntity(person);
        }
    }

    private ResponseEntity<PersonAuthorizeDto> responseEntity(PersonAuthorizeDto person) {
        String URL = "http://localhost:8081/user/authorize-response";
        person.setTokenSession(UUID.randomUUID());
        return restTemplate.postForEntity(URL, person, PersonAuthorizeDto.class);

    }

}


