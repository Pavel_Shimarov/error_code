package ru.sber.entities;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Accessors(chain = true)
@Data
@Entity
@DiscriminatorColumn(name = "product_type")
public class ProductEntity {

    @Id
    @Column(name = "id_product")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer id;

    @Column(name = "type_product")
    private String type;

    @Column(name = "sub_type_product")
    private String subType;

    @Column(name = "name_product")
    private String productName;

    @Column(name = "price_product")
    private Integer price;

    @Column(name = "volume_product")
    private Integer volume;

    @Column(name = "product_uuid")
    private UUID productUUID;

    @ManyToOne
    @JoinColumn(name = "description_id")
    private DescriptionEntity BD_TYPE;


//    @ManyToOne
//    @JoinColumn(name = "person_id")
//    DescriptionEntity personId;


}
