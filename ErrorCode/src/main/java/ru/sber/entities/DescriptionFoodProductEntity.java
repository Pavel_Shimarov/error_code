package ru.sber.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@DiscriminatorValue("FP")
public class DescriptionFoodProductEntity extends DescriptionEntity {

    @Column
    private Integer shelfLifeDay;
}
