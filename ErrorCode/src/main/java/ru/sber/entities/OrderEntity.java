package ru.sber.entities;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Accessors(chain = true)
@Data
@Entity
@Table(name = "order_table")
public class OrderEntity {

    @Id
    @Column(name = "order_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_price")
    private Integer productPrice;

    @Column(name = "product_value")
    private Integer productValue;

    @Column(name = "product_uuid")
    private UUID productUUID;

    @Column(name = "person_uuid")
    private UUID personUUID;

    @Column(name = "sold_order")
    private String sold;
}
