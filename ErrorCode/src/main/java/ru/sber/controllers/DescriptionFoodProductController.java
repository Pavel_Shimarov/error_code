package ru.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.DescriptionFoodProductDto;
import ru.sber.exceptions.*;
import ru.sber.services.DescriptionFoodProductService;
import ru.sber.utils.exception.ControllersException;

@RestController
@RequiredArgsConstructor
public class DescriptionFoodProductController {

    private final DescriptionFoodProductService descriptionFoodProductService;
    private final ControllersException controllersException;

    public ResponseEntity<DescriptionFoodProductDto> save(@RequestBody DescriptionFoodProductDto dto) throws Exception {
        ResponseEntity<DescriptionFoodProductDto> ok = ResponseEntity.ok(descriptionFoodProductService.save(dto));
        controllersException.throwBusinessException(ok);
        return ok;
    }

    public ResponseEntity<DescriptionFoodProductDto> find(@PathVariable Integer id) throws Exception {
        ResponseEntity<DescriptionFoodProductDto> ok = ResponseEntity.ok(descriptionFoodProductService.getById(id));
        if (descriptionFoodProductService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        return ok;
    }

    public void deleteById(@PathVariable Integer id) throws Exception {
        ResponseEntity<DescriptionFoodProductDto> ok = ResponseEntity.ok(descriptionFoodProductService.getById(id));
        if (descriptionFoodProductService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        descriptionFoodProductService.delete(id);
    }

    public void putById(@PathVariable Integer id, @RequestBody DescriptionFoodProductDto dto) {
        DescriptionFoodProductDto byId = descriptionFoodProductService.getById(id);
        byId.setShelfLifeDay(dto.getShelfLifeDay());
        descriptionFoodProductService.save(byId);
    }

    @ExceptionHandler(NoFoundException.class)
    public static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    public ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    public ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    public ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
