package ru.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.ProductDto;
import ru.sber.enums.PersonVariable;
import ru.sber.exceptions.*;
import ru.sber.services.ProductService;
import ru.sber.utils.exception.ControllersException;

import java.util.List;
import java.util.UUID;

/*Основной контроллер для адмнистратора*/

@RestController
@RequestMapping("/administrator")
@RequiredArgsConstructor
public class AdministratorController {

    private final ProductService productService;
    private final ControllersException controllersException;

    @PostMapping("/save/{token}")
    public Object save(@RequestBody ProductDto dto, @PathVariable UUID token) {

        try {
            exceptionSession(token);
        } catch (IncorrectInputException e) {
            return e.getMessage();
        }
        ResponseEntity<ProductDto> ok = ResponseEntity.ok(productService.save(dto));
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }

    @GetMapping("/findAllProduct/{token}")
    public Object findAll(@PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (IncorrectInputException e) {
            return e.getMessage();
        }
        ResponseEntity<List<ProductDto>> ok = ResponseEntity.ok(productService.getAll());
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }

    @GetMapping("/findProductById/{token}/{id}")
    public Object find(@PathVariable Integer id, @PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (IncorrectInputException e) {
            return e.getMessage();
        }
        ResponseEntity<ProductDto> ok = ResponseEntity.ok(productService.getById(id));
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }

    @DeleteMapping("/deleteProductById/{token}/{id}")
    public Object deleteById(@PathVariable Integer id, @PathVariable UUID token) {
        find(id, token);
        productService.delete(id);
        return productService.getById(id).getProductName() + " удален";
    }
//изменение продукта
    @PutMapping("/putById/{token}/{id}")
    public void putById(@PathVariable Integer id, @RequestBody ProductDto dto, @PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (IncorrectInputException e) {
            e.printStackTrace();
        }
        ProductDto byId = productService.getById(id);
        byId.setType(dto.getType());
        byId.setSubType(dto.getSubType());
        byId.setProductName(dto.getProductName());
        byId.setPrice(dto.getPrice());
        byId.setVolume(dto.getVolume());
        byId.setProductUUID(dto.getProductUUID());
        productService.save(byId);
    }

    private void exceptionSession(UUID token) throws IncorrectInputException {
        if (!BusinessControllers.getSessionMap().containsKey(token)) {
            throw new IncorrectInputException("Пользователь не найден");
        } else if (System.currentTimeMillis() > BusinessControllers.getSessionMap().get(token).getTimeDeathToken()) {
            throw new IncorrectInputException("Время сессии истекло");
        } else if (!BusinessControllers.getSessionMap().get(token).getVariable().equals(PersonVariable.ADMINISTRATOR.toString())) {
            throw new IncorrectInputException("no ADMINISTRATOR");
        }
    }

    @ExceptionHandler(NoFoundException.class)
    private static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    private ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    private ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    private ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    private ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
