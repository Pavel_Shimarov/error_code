package ru.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.DescriptionTechProductDto;
import ru.sber.exceptions.*;
import ru.sber.services.DescriptionTechProductService;
import ru.sber.utils.exception.ControllersException;


@RestController
@RequestMapping("/descriptionTech")
@RequiredArgsConstructor
public class DescriptionTechProductController {

    private final DescriptionTechProductService descriptionTechProductService;
    private final ControllersException controllersException;

    @PostMapping("/save/")
    public ResponseEntity<DescriptionTechProductDto> save(@RequestBody DescriptionTechProductDto dto) throws Exception {
        ResponseEntity<DescriptionTechProductDto> ok = ResponseEntity.ok(descriptionTechProductService.save(dto));
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<DescriptionTechProductDto> find(@PathVariable Integer id) throws Exception {
        ResponseEntity<DescriptionTechProductDto> ok = ResponseEntity.ok(descriptionTechProductService.getById(id));
        if (descriptionTechProductService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        return ok;
    }


    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable Integer id) throws Exception {
        ResponseEntity<DescriptionTechProductDto> ok = ResponseEntity.ok(descriptionTechProductService.getById(id));
        if (descriptionTechProductService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        descriptionTechProductService.delete(id);
    }

    @PutMapping("/putById/{id}")
    public void putById(@PathVariable Integer id, @RequestBody DescriptionTechProductDto dto) {
        DescriptionTechProductDto byId = descriptionTechProductService.getById(id);
        byId.setPower(dto.getPower());
        descriptionTechProductService.save(byId);
    }

    @ExceptionHandler(NoFoundException.class)
    public static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    public ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    public ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    public ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
