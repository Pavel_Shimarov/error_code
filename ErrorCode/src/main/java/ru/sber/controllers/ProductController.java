package ru.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.ProductDto;
import ru.sber.exceptions.*;
import ru.sber.services.ProductService;
import ru.sber.utils.exception.ControllersException;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ControllersException controllersException;

    @PostMapping("/save")
    public ResponseEntity<ProductDto> save(@RequestBody ProductDto dto) throws Exception {
        ResponseEntity<ProductDto> ok = ResponseEntity.ok(productService.save(dto));
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<ProductDto>> findAll() throws Exception {
        ResponseEntity<List<ProductDto>> ok = ResponseEntity.ok(productService.getAll());
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<ProductDto> find(@PathVariable Integer id) throws Exception {
        ResponseEntity<ProductDto> ok = ResponseEntity.ok(productService.getById(id));
        if (productService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable Integer id) throws Exception {
        if (productService.getById(id) != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ResponseEntity.ok(productService.getById(id)));
        productService.delete(id);
    }

    @PutMapping("/putById/{id}")
    public void putById(@PathVariable Integer id, @RequestBody ProductDto dto) {
        ProductDto byId = productService.getById(id);
        byId.setType(dto.getType());
        byId.setSubType(dto.getSubType());
        byId.setProductName(dto.getProductName());
        byId.setPrice(dto.getPrice());
        byId.setVolume(dto.getVolume());
        byId.setBD_TYPE(dto.getBD_TYPE());
        byId.setProductUUID(dto.getProductUUID());
        productService.save(byId);
    }

    @ExceptionHandler(NoFoundException.class)
    public static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    public ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    public ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    public ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
