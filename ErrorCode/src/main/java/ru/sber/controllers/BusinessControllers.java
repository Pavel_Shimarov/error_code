package ru.sber.controllers;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.ChekDto;
import ru.sber.dto.OrderDto;
import ru.sber.dto.PersonDto;
import ru.sber.dto.ProductDto;
import ru.sber.enums.PersonVariable;
import ru.sber.exceptions.*;
import ru.sber.services.OrderService;
import ru.sber.services.ProductService;
import ru.sber.services.StoreService;
import ru.sber.utils.exception.ControllersException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/*Основной контроллер для покупателя*/

@RestController
@RequestMapping("/user")
@Data
public class BusinessControllers {

    private final ProductService productService;
    private final OrderService orderService;
    private final ControllersException controllersException;
    private final OrderController orderController;
    private final StoreService storeService;

    private ChekDto chekDto;

    private static HashMap<UUID, PersonDto> sessionMap = new HashMap<>();
    private final Long TIME_LIFE_SESSION = 1000_000L;//время жизни сессии
    //15 мин = 900_000L

    //принимает токен и персона от приложения authorization
    @PostMapping("/authorize-response")
    public void authorize(@RequestBody PersonDto responsePerson) {
        //записываем время жизни токена в PersonDto
        responsePerson.setTimeDeathToken(System.currentTimeMillis() + TIME_LIFE_SESSION);
        //getPersonUUID()
        //поиск в маппе-сессии по значению: PersonDto и изменить время жизни на просраченное
        for (Map.Entry<UUID, PersonDto> pair : sessionMap.entrySet()) {
            if (responsePerson.getPersonUuid().equals(pair.getValue().getPersonUuid())) {
                sessionMap.get(pair.getKey()).setTimeDeathToken(System.currentTimeMillis());
            }
        }
        //записываем в маппу-сессии ключ: токен, значение: PersonDto
        sessionMap.put(responsePerson.getTokenSession(), responsePerson);
        System.out.println("Входящий клиент: " + responsePerson);
        System.out.println("Карта ссессии : " + sessionMap);
    }

    //пользователь просматривает каталлог
    @GetMapping("/find-all-product/{token}")
    public Object findAllProduct(@PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }
        ResponseEntity<List<ProductDto>> ok =
                ResponseEntity.ok(productService.getAll());
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }
//пользователь просматривает каталлог техники
    @GetMapping("/find-tech-product/{token}")
    public Object findTechProduct(@PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }
        ResponseEntity<List<ProductDto>> ok =
                ResponseEntity.ok(productService.getTechProduct());
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }
    //пользователь просматривает каталлог продуктов
    @GetMapping("/find-food-product/{token}")
    public Object findFoodProduct(@PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }
        ResponseEntity<List<ProductDto>> ok =
                ResponseEntity.ok(productService.getFoodProduct());
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }

    //поиск продукта по части наименования
    @GetMapping("/find-like-product/{token}/{productName}")
    public Object findProductWithPartOfName(@PathVariable String productName, @PathVariable UUID token) {
        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }
        ResponseEntity<List<ProductDto>> ok =
                ResponseEntity.ok(productService.findProductWithPartOfName(productName));
        try {
            controllersException.throwBusinessException(ok);
        } catch (Exception e) {
            return e.getMessage();
        }
        return ok;
    }

    //пользователь вводит количество товара котое хочет купить и нажимает кнопку положить в корзину
    @PostMapping("/create-order/{token}")
    public Object createOrder(@RequestBody OrderDto order, @PathVariable UUID token) throws Exception {

        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }
        if (productService.getByProductUUID(order.getProductUUID()).getPrice() == null) {
            return new IncorrectInputException("Продукт не найден").getMessage();
        }
        if (order.getProductValue() == null) {
            return new IncorrectInputException("Вы не ввели объем покупки").getMessage();
        }
        if (order.getProductValue() > (productService.getByProductUUID(order.getProductUUID()).getVolume())) {
            return new IncorrectInputException("Объем заказа превышает остатки на складе").getMessage();
        }
        //сохраняем токен в ордер
        order.setSessionToken(token);
        //ищем в мап сессии по токену персона вставляем в ордер токен UUID
        order.setPersonUUID(sessionMap.get(token).getPersonUuid());
        order.setProductName(productService.getByProductUUID(order.getProductUUID()).getProductName());
        order.setProductPrice(productService.getByProductUUID(order.getProductUUID()).getPrice());
        order.setSold("Не оплачен");
        //сохраняем ордер в базу
        ResponseEntity<OrderDto> ok = ResponseEntity.ok(orderService.save(order));
        controllersException.throwBusinessException(ok);
        return ok;
    }

    //просмотр корзины
    @GetMapping("/findBasket/{token}")
    public Object findAll(@PathVariable UUID token) throws Exception {

        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }

        List<OrderDto> bySoldAndPersonUUID = orderService
                .findByPersonUUID(sessionMap.get(token).getPersonUuid());
        ResponseEntity<List<OrderDto>> ok = ResponseEntity.ok(bySoldAndPersonUUID);
        controllersException.throwBusinessException(ok);
        return ok;
    }

    //пользователь переходит в корзину и нажимает кнопку оплатить(купить)
    @GetMapping("/sold-order/{token}")
    public String buy(@PathVariable UUID token) throws Exception {
        try {
            exceptionSession(token);
        } catch (Exception e) {
            return e.getMessage();
        }
        int sum = 0;
        //вытаскиваем из базы лист ордеров по персон UUID
        List<OrderDto> bySoldAndPersonUUID = orderService
                .findBySoldAndPersonUUID("Не оплачен", sessionMap.get(token).getPersonUuid());
        //нужно пробежаться по листу ордеров, сравнить объем заказа в каждом ордере с объемом на остататках
        if ((findValueOrder(bySoldAndPersonUUID)) != null) {
            return new IncorrectInputException("Необходимого количества товара (" + findValueOrder(bySoldAndPersonUUID) + ") нет на складе")
                    .getMessage();
        }
        if (bySoldAndPersonUUID.size() == 0) {
            return new IncorrectInputException("Нет не оплаченных ордеров").getMessage();
        }
        //умножаем купленное количество на цену товара, суммируем стоимости покупок
        for (OrderDto soldOrder : bySoldAndPersonUUID) {
            System.out.println("Цена " + soldOrder.getProductPrice());
            System.out.println("Колличество " + soldOrder.getProductValue());
            sum = sum + soldOrder.getProductPrice() * soldOrder.getProductValue();
            //помечаем ордер как исполненный
            OrderDto orderDto = soldOrder.setSold("Оплачен");
            orderService.save(orderDto);
            //уменьшаем остатки товара на складе
            changeProductValue(soldOrder);
        }
        //записываем в чек лист оплаченных ордеров и итоговую сумму
        ChekDto chekDto = new ChekDto();
        chekDto
                .setOrderDtoList(bySoldAndPersonUUID)
                .setSumSold(sum);
        //печать чека
        String s = writeFileBuy(chekDto);

        ResponseEntity<List<OrderDto>> ok = ResponseEntity.ok(bySoldAndPersonUUID);
        controllersException.throwBusinessException(ok);
        return s;
    }

    public static HashMap<UUID, PersonDto> getSessionMap() {
        return sessionMap;
    }

    //метод для умньшения количества продуктов на складе после покупки
    private void changeProductValue(OrderDto order) {
        ProductDto product = productService.getByProductUUID(order.getProductUUID());
        product.setVolume(product.getVolume() - order.getProductValue());
        productService.save(product);
    }

    private String writeFileBuy(ChekDto chekDto) {//формируем чек
        StringBuilder str = new StringBuilder();
        for (OrderDto order : chekDto.getOrderDtoList()) {
            str.append(String.format("\n Куплено %s штук %s по цене %s",
                    order.getProductValue(),
                    order.getProductName(),
                    order.getProductPrice()));
        }

        return "\nПродавец: " + storeService.getById(1).getNameStore()
                + "\n"
                + "Товар: " + "\n" + str
                + "\n"
                + "\n"
                + "Итого: " + chekDto.getSumSold()
                + "\n";
    }
//проверка количества товара на остатках перед покупкой
    private String findValueOrder(List<OrderDto> orders) {
        for (OrderDto order : orders) {
            if (order.getProductValue() > (productService
                    .getByProductUUID(order.getProductUUID())
                    .getVolume())) {
                return order.getProductName();//если в заказе объем заказанного товара больше чем на остатках, то true
            }
        }
        return null;
    }
//исключения при проверке сессии
    private void exceptionSession(UUID token) throws IncorrectInputException {
        if (!BusinessControllers.getSessionMap().containsKey(token)) {
            throw new IncorrectInputException("Пользователь не найден");
        } else if (System.currentTimeMillis() > BusinessControllers.getSessionMap().get(token).getTimeDeathToken()) {
            throw new IncorrectInputException("Время сессии истекло");
        } else if (!BusinessControllers.getSessionMap().get(token).getVariable().equals(PersonVariable.USER.toString())) {
            throw new IncorrectInputException("не USER");
        }
    }

    @ExceptionHandler(NoFoundException.class)
    private static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    private ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    private ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    private ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    private ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
