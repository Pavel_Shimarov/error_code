package ru.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.OrderDto;
import ru.sber.exceptions.*;
import ru.sber.services.OrderService;
import ru.sber.utils.exception.ControllersException;

import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final ControllersException controllersException;

    @PostMapping("/save/")
    public ResponseEntity<OrderDto> save(@RequestBody OrderDto dto) throws Exception {
        ResponseEntity<OrderDto> ok = ResponseEntity.ok(orderService.save(dto));
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<OrderDto> find(@PathVariable Integer id) throws Exception {
        ResponseEntity<OrderDto> ok = ResponseEntity.ok(orderService.getById(id));
        if (orderService.getById(id).getId() != null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @GetMapping("/findAll/")
    public ResponseEntity<List<OrderDto>> findAll() throws Exception {
        ResponseEntity<List<OrderDto>> ok = ResponseEntity.ok(orderService.getAll());
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable Integer id) throws Exception {
        ResponseEntity<OrderDto> ok = ResponseEntity.ok(orderService.getById(id));
        if (orderService.getById(id).getId() == null) {
            throw new NoFoundException();
        }
        controllersException.throwBusinessException(ok);
        orderService.delete(id);
    }

    @PutMapping("/putById/{id}")
    public void putById(@PathVariable Integer id, @RequestBody OrderDto dto) {
        OrderDto byId = orderService.getById(id);
        byId.setProductName(dto.getProductName());
        byId.setProductPrice(dto.getProductPrice());
        byId.setProductValue(dto.getProductValue());
        byId.setProductUUID(dto.getProductUUID());
        byId.setPersonUUID(dto.getPersonUUID());
        orderService.save(byId);
    }

    @ExceptionHandler(NoFoundException.class)
    public static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    public ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    public ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    public ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
