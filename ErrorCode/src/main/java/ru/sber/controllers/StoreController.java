package ru.sber.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.StoreDto;
import ru.sber.exceptions.*;
import ru.sber.services.StoreService;
import ru.sber.utils.exception.ControllersException;

import java.util.List;

@RestController
@RequestMapping("/store")
@RequiredArgsConstructor
public class StoreController {

    private final StoreService storeService;
    private final ControllersException controllersException;

    @PostMapping("/save/")
    public ResponseEntity<StoreDto> save(@RequestBody StoreDto storeDto) throws Exception {
        ResponseEntity<StoreDto> ok = ResponseEntity.ok(storeService.save(storeDto));
        controllersException.throwBusinessException(ok);
        return ok;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<StoreDto>> findAll() throws Exception {
        ResponseEntity<List<StoreDto>> ok = ResponseEntity.ok(storeService.getAll());
        controllersException.throwBusinessException(ok);
        if (storeService.getAll().get(0).getId() == null) {
            throw new NoFoundException();
        }
        return ok;
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<StoreDto> find(@PathVariable Integer id) throws Exception {
        ResponseEntity<StoreDto> ok = ResponseEntity.ok(storeService.getById(id));
        controllersException.throwBusinessException(ok);
        if (storeService.getById(id).getId() == null) {
            throw new NoFoundException();
        }
        return ok;
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable Integer id) throws NoFoundException {
        if (storeService.getById(id) == null) {
            throw new NoFoundException();
        }
        storeService.delete(id);
    }

    @PutMapping("/putById/{id}")
    public void putById(@PathVariable Integer id, @RequestBody StoreDto dto) throws Exception {
        StoreDto byId = storeService.getById(id);
        byId.setNameStore(dto.getNameStore());
        storeService.save(byId);
    }

    @ExceptionHandler(NoFoundException.class)
    public static ResponseEntity noFoundExceptionHandler(NoFoundException noFoundException) {
        return ResponseEntity.status(noFoundException.getHttpStatus()).build();
    }

    @ExceptionHandler(UnknownException.class)
    public ResponseEntity unknownExceptionHandler(UnknownException unknownException) {
        return ResponseEntity.status(unknownException.getHttpStatus()).build();
    }

    @ExceptionHandler(MovedTemporarilyException.class)
    public ResponseEntity unknownExceptionHandler(MovedTemporarilyException movedTemporarilyException) {
        return ResponseEntity.status(movedTemporarilyException.getHttpStatus()).build();
    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity unknownExceptionHandler(InternalServerError serverError) {
        return ResponseEntity.status(serverError.getHttpStatus()).build();
    }

    @ExceptionHandler(IncorrectInputException.class)
    public ResponseEntity unknownExceptionHandler(IncorrectInputException incorrectInputException) {
        return ResponseEntity.status(incorrectInputException.getHttpStatus()).build();
    }
}
