package ru.sber.utils.cleaner;

import org.springframework.stereotype.Service;
import ru.sber.controllers.BusinessControllers;
import ru.sber.dto.PersonDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@Service
public class CleanerSession {

    public void clearMapSession() {
        Thread thread = new Thread(
                () -> {
                    while (true) {
                        try {
                            Thread.sleep(1_000_000);
                            System.out.println("Очистил мапу сессии");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        HashMap<UUID, PersonDto> sessionMap = BusinessControllers.getSessionMap();
                        ArrayList<UUID> values = new ArrayList<>(sessionMap.keySet());
                        for (int i = 0; i < values.size(); i++) {
                            if (System.currentTimeMillis() > sessionMap.get(values.get(i)).getTimeDeathToken()) {
                                sessionMap.remove(values.get(i));
                            }
                        }
                    }
                }
        );
        thread.setDaemon(true);
        thread.start();
    }
}
