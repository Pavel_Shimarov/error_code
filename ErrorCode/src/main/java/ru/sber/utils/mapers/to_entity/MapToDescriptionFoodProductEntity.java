package ru.sber.utils.mapers.to_entity;

import org.springframework.stereotype.Service;
import ru.sber.dto.DescriptionFoodProductDto;
import ru.sber.entities.DescriptionFoodProductEntity;

@Service
public class MapToDescriptionFoodProductEntity {

    public DescriptionFoodProductEntity map(DescriptionFoodProductDto dto) {
        DescriptionFoodProductEntity entity = new DescriptionFoodProductEntity();
        entity
                .setShelfLifeDay(dto.getShelfLifeDay())
                .setId(dto.getId());

        return entity;
    }
}
