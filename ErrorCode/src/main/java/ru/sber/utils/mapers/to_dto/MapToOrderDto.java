package ru.sber.utils.mapers.to_dto;

import org.springframework.stereotype.Service;
import ru.sber.dto.OrderDto;
import ru.sber.entities.OrderEntity;

@Service
public class MapToOrderDto {

    public OrderDto mapToDto(OrderEntity entity) {
        OrderDto dto = new OrderDto()
                .setId(entity.getId())
                .setProductName(entity.getProductName())
                .setProductPrice(entity.getProductPrice())
                .setProductValue(entity.getProductValue())
                .setPersonUUID(entity.getPersonUUID())
                .setProductUUID(entity.getProductUUID())
                .setSold(entity.getSold());
        return dto;
    }
}
