package ru.sber.utils.mapers.to_entity;

import org.springframework.stereotype.Service;
import ru.sber.dto.StoreDto;
import ru.sber.entities.StoreEntity;

@Service
public class MapToStoreEntity {

    public StoreEntity map(StoreDto dto) {
        StoreEntity entity = new StoreEntity()
                .setId(dto.getId())
                .setStoreName(dto.getNameStore());
        return entity;
    }

}
