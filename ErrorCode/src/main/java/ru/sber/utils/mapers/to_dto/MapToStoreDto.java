package ru.sber.utils.mapers.to_dto;

import org.springframework.stereotype.Service;
import ru.sber.dto.StoreDto;
import ru.sber.entities.StoreEntity;

@Service
public class MapToStoreDto {

    public StoreDto mapToDto(StoreEntity entity) {
        StoreDto dto = new StoreDto()
                .setId(entity.getId())
                .setNameStore(entity.getStoreName());
        return dto;
    }

}
