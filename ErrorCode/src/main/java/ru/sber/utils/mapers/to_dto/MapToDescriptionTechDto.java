package ru.sber.utils.mapers.to_dto;

import org.springframework.stereotype.Service;
import ru.sber.dto.DescriptionTechProductDto;
import ru.sber.entities.DescriptionTechProductEntity;

@Service
public class MapToDescriptionTechDto {
    public DescriptionTechProductDto mapToDto(DescriptionTechProductEntity entity) {
        DescriptionTechProductDto dto = new DescriptionTechProductDto();
        dto
                .setPower(entity.getPower())
                .setId(entity.getId());
        return dto;
    }
}
