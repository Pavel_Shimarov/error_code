package ru.sber.utils.mapers.to_entity;

import org.springframework.stereotype.Service;
import ru.sber.dto.OrderDto;
import ru.sber.entities.OrderEntity;

@Service
public class MapToOrderEntity {

    public OrderEntity map(OrderDto dto) {
        OrderEntity entity = new OrderEntity()
                .setId(dto.getId())
                .setProductName(dto.getProductName())
                .setProductPrice(dto.getProductPrice())
                .setProductValue(dto.getProductValue())
                .setProductUUID(dto.getProductUUID())
                .setPersonUUID(dto.getPersonUUID())
                .setSold(dto.getSold());
        return entity;
    }
}
