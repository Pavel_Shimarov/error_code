package ru.sber.utils.mapers.to_dto;

import org.springframework.stereotype.Service;
import ru.sber.dto.DescriptionFoodProductDto;
import ru.sber.entities.DescriptionFoodProductEntity;

@Service
public class MapToDescriptionFoodProductDto {

    public DescriptionFoodProductDto mapToDto(DescriptionFoodProductEntity entity) {
        DescriptionFoodProductDto dto = new DescriptionFoodProductDto();
        dto
                .setShelfLifeDay(entity.getShelfLifeDay())
                .setId(entity.getId());
        return dto;
    }
}
