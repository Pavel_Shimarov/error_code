package ru.sber.utils.mapers.to_entity;

import org.springframework.stereotype.Service;
import ru.sber.dto.DescriptionTechProductDto;
import ru.sber.entities.DescriptionTechProductEntity;

@Service
public class MapToDescriptionTechProductEntity {

    public DescriptionTechProductEntity map(DescriptionTechProductDto dto) {
        DescriptionTechProductEntity entity = new DescriptionTechProductEntity();
        entity
                .setPower(dto.getPower())
                .setId(dto.getId());

        return entity;
    }
}
