package ru.sber.utils.mapers.to_dto;

import org.springframework.stereotype.Service;
import ru.sber.dto.ProductDto;
import ru.sber.entities.ProductEntity;

@Service
public class MapToProductDto {

    public ProductDto mapToDto(ProductEntity entity) {
        ProductDto dto = new ProductDto()
                .setId(entity.getId())
                .setProductName(entity.getProductName())
                .setType(entity.getType())
                .setSubType(entity.getSubType())
                .setPrice(entity.getPrice())
                .setVolume(entity.getVolume())
                .setBD_TYPE(entity.getBD_TYPE())
                .setProductUUID(entity.getProductUUID());
        return dto;
    }
}
