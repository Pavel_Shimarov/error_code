package ru.sber.utils.mapers.to_entity;

import org.springframework.stereotype.Service;
import ru.sber.dto.ProductDto;
import ru.sber.entities.ProductEntity;

@Service
public class MapToProductEntity {

    public ProductEntity map(ProductDto dto) {
        ProductEntity entity = new ProductEntity()
                .setId(dto.getId())
                .setType(dto.getType())
                .setSubType(dto.getSubType())
                .setProductName(dto.getProductName())
                .setVolume(dto.getVolume())
                .setPrice(dto.getPrice())
                .setProductUUID(dto.getProductUUID())
                .setBD_TYPE(dto.getBD_TYPE());
        return entity;
    }
}
