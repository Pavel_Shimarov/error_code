package ru.sber.utils.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.sber.exceptions.IncorrectInputException;
import ru.sber.exceptions.MovedTemporarilyException;
import ru.sber.exceptions.UnknownException;

@Service
@RequiredArgsConstructor
public class ControllersException {

    public <T> void throwBusinessException(ResponseEntity<T> responseEntity) throws Exception {
        if (responseEntity.getStatusCodeValue() == 520) {
            throw new UnknownException(responseEntity.toString());
        } else if (responseEntity.getStatusCodeValue() == 400) {
            throw new IncorrectInputException(responseEntity.toString());
        } else if (responseEntity.getStatusCodeValue() == 302) {
            throw new MovedTemporarilyException();
        } else if (responseEntity.getStatusCodeValue() == 500) {
            throw new IncorrectInputException(responseEntity.toString());
        }
    }
}
