package ru.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.aspect.LoggableAnnotation;
import ru.sber.entities.DescriptionEntity;
import ru.sber.entities.ProductEntity;
import ru.sber.entities.StoreEntity;
import ru.sber.repositories.DescriptionRepository;
import ru.sber.repositories.ProductRepository;
import ru.sber.repositories.StoreRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FillerUtil {
    private final ProductRepository productRepository;
    private final DescriptionRepository descriptionRepository;
    private final StoreRepository storeRepository;


    public void addProductOne(DescriptionEntity description, ProductEntity product) {
        DescriptionEntity saveDescription = descriptionRepository.save(description);
        product.setBD_TYPE(saveDescription);
        productRepository.save(product);
    }


    public void addProductAll(List<DescriptionEntity> descriptions, List<ProductEntity> products) {
        int count = 0;
        for (ProductEntity p : products) {
            if (count > descriptions.size() - 1) {
                break;
            }
            p.setBD_TYPE(descriptions.get(count));
            count++;
        }
        descriptionRepository.saveAll(descriptions);
        productRepository.saveAll(products);
    }


    public void addStore(List<StoreEntity> store) {
        storeRepository.saveAll(store);
    }

    @LoggableAnnotation
    public List<StoreEntity> storeLogs(){
       return storeRepository.findAll();
    }

    @LoggableAnnotation
    public List<ProductEntity> productLogs(){
        return productRepository.findAll();
    }
}
