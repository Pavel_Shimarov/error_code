package ru.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import ru.sber.aspect.LoggableAnnotation;
import ru.sber.entities.DescriptionEntity;
import ru.sber.entities.ProductEntity;
import ru.sber.entities.StoreEntity;
import ru.sber.repositories.DescriptionRepository;
import ru.sber.repositories.ProductRepository;
import ru.sber.repositories.StoreRepository;
import ru.sber.utils.cleaner.CleanerSession;

import java.util.List;


@Service
@RequiredArgsConstructor
public class InitiateUtil implements CommandLineRunner {

    private final StoreRepository storeRepository;
    private final ProductRepository productRepository;
    private final DescriptionRepository descriptionRepository;
    private final FillerUtil fillerUtil;

    private final VolumeStoreTable volumeStoreTable;
    private final VolumeFoodTable volumeFoodTable;
    private final VolumeTechTable volumeTechTable;
    private final CleanerSession cleanerSession;


    @Override
    public void run(String... args) {

        volumeStoreTable.fillVolumeStoreTable();
        volumeFoodTable.fillList();
        volumeTechTable.fillList();

        System.out.println("\n   Таблица магазинов");
        for (StoreEntity person : storeRepository.findAll()) {
            System.out.println(person);
        }


        System.out.println("\n   Таблица продуктов");
        for (ProductEntity product : productRepository.findAll()) {
            System.out.println(product.toString());
        }

        System.out.println("\n   Таблица харрактеристик");
        for (DescriptionEntity description : descriptionRepository.findAll()) {
            System.out.println(description);
        }

        cleanerSession.clearMapSession();//запуск шедулера по очистке кеша сессии

        fillerUtil.productLogs();
        fillerUtil.storeLogs();

    }
}

