package ru.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.entities.DescriptionEntity;
import ru.sber.entities.DescriptionTechProductEntity;
import ru.sber.entities.ProductEntity;
import ru.sber.enums.ProductTechSubType;
import ru.sber.enums.ProductType;

import java.util.*;

@Service
@RequiredArgsConstructor
public class VolumeTechTable {

    private final FillerUtil fillerUtil;

    List<ProductEntity> productTechList = new ArrayList<>(
            Arrays.asList(
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.PHONE.toString()).setProductName("LG-130").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.TV.toString()).setProductName("TV-340").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.WASHING_MACHINE.toString()).setProductName("Zinger-76").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.PHONE.toString()).setProductName("Gnusmus").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.TV.toString()).setProductName("Xamomi").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.WASHING_MACHINE.toString()).setProductName("Purpul").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.PHONE.toString()).setProductName("Peppel").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.TV.toString()).setProductName("Zenit").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.WASHING_MACHINE.toString()).setProductName("Inergit").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.PHONE.toString()).setProductName("Pokio").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.TV.toString()).setProductName("Jkubovich").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.WASHING_MACHINE.toString()).setProductName("Poloskun").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.PHONE.toString()).setProductName("Smolny").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.TV.toString()).setProductName("Zombie-box").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.WASHING_MACHINE.toString()).setProductName("River").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.PHONE.toString()).setProductName("Meduze").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.TV.toString()).setProductName("Torrent").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.TECHNIC.toString()).setSubType(ProductTechSubType.WASHING_MACHINE.toString()).setProductName("Mammy").setPrice(randomPositiveNumber(3000, 10000)).setVolume(randomPositiveNumber(100, 900)).setProductUUID(UUID.randomUUID())
            )
    );

    List<DescriptionEntity> descriptionTechProductEntities = new ArrayList<>(
            Arrays.asList(
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W"),
                    new DescriptionTechProductEntity().setPower(randomPositiveNumber(30, 200) + "W")
            )
    );

    public void fillList() {
        fillerUtil.addProductAll(descriptionTechProductEntities, productTechList);
    }

    private static Integer randomPositiveNumber(Integer ot, Integer dot) {
        return Math.abs(new Random().nextInt() % dot) + ot;
    }
}
