package ru.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.entities.StoreEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VolumeStoreTable {

    private final FillerUtil fillerUtil;

    private List<StoreEntity> getStoreList() {

        return new ArrayList<>(
                Arrays.asList(
                        new StoreEntity()
                                .setStoreName("Онлайн Магазин")
                )
        );
    }

    public void fillVolumeStoreTable() {
        List<StoreEntity> storeEntities = getStoreList();
        fillerUtil.addStore(storeEntities);
    }
}
