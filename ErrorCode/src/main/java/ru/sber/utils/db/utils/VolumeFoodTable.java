package ru.sber.utils.db.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.entities.DescriptionEntity;
import ru.sber.entities.DescriptionFoodProductEntity;
import ru.sber.entities.ProductEntity;
import ru.sber.enums.ProductFoodSubType;
import ru.sber.enums.ProductType;

import java.util.*;

@Service
@RequiredArgsConstructor
public class VolumeFoodTable {

    private final FillerUtil fillerUtil;

    List<ProductEntity> productFoodList = new ArrayList<>(
            Arrays.asList(
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Ананас").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Яблоко").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Вишня").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Слива").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Апельсин").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Банан").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Груша").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Мандарин").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Алыча").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID()),
                    new ProductEntity().setType(ProductType.FOOD.toString()).setSubType(ProductFoodSubType.FRUIT.toString()).setProductName("Виноград").setPrice(randomPositiveNumber(100)).setVolume(randomPositiveNumber(100)).setProductUUID(UUID.randomUUID())
            )
    );

    List<DescriptionEntity> descriptionFoodProductEntities = new ArrayList<>(
            Arrays.asList(
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20)),
                    new DescriptionFoodProductEntity().setShelfLifeDay(randomPositiveNumber(20))
            )
    );

    public void fillList() {
        fillerUtil.addProductAll(descriptionFoodProductEntities, productFoodList);
    }

    private static Integer randomPositiveNumber(Integer to) {
        return Math.abs(new Random().nextInt() % to) + 1;
    }
}
