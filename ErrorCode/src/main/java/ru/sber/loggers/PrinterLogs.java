package ru.sber.loggers;

import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;

@Service
public class PrinterLogs {

    public void writeLogByFile(String log) {
        String path = "BusinessLogs.txt";
        try (FileWriter output = new FileWriter(path, true)) {
            output.write(String.format("\n%s", log));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
