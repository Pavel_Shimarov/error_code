package ru.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class OrderDto {

    private Integer id;

    private String productName;

    private Integer productPrice;

    private Integer productValue;

    private UUID productUUID;

    private UUID personUUID;

    private UUID sessionToken;

    private String sold;

}
