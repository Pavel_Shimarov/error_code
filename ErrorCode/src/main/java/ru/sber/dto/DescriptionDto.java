package ru.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class DescriptionDto {

    private Integer id;
}
