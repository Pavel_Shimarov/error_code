package ru.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.sber.entities.DescriptionEntity;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class ProductDto {

    private Integer id;

    private String type;

    private String subType;

    private String productName;

    private Integer price;

    private Integer volume;

    private UUID productUUID;

    private DescriptionEntity BD_TYPE;
}

