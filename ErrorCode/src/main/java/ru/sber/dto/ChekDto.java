package ru.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ChekDto {

    private List<OrderDto> orderDtoList;

    private Integer sumSold;
}
