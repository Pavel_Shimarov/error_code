package ru.sber.dto;

import lombok.Data;

import java.util.UUID;


@Data
public class PersonDto {

    private Integer id;

    private String variable;

    private String name;

    private UUID personUuid;

    private String login;

    private String password;

    private UUID tokenSession;

    private Long timeDeathToken;

}
