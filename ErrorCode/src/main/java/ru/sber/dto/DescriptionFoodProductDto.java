package ru.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DescriptionFoodProductDto extends DescriptionDto {

    private Integer shelfLifeDay;
}
