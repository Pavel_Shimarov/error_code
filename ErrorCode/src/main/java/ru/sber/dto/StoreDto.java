package ru.sber.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class StoreDto {

    private Integer id;

    private String nameStore;
}
