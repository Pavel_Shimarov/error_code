package ru.sber.exceptions;

import lombok.Data;

@Data
public class IncorrectInputException extends Exception {

    private String message;

    private Integer httpStatus = 400;

    public IncorrectInputException(String message) {
        this.message = "400 Bad Request (" + message + " " + httpStatus + ")";
    }
}
