package ru.sber.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.dto.ProductDto;
import ru.sber.entities.ProductEntity;
import ru.sber.enums.ProductType;
import ru.sber.repositories.ProductRepository;
import ru.sber.utils.mapers.to_dto.MapToProductDto;
import ru.sber.utils.mapers.to_entity.MapToProductEntity;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final MapToProductDto mapToProductDto;
    private final MapToProductEntity mapToProductEntity;

    public ProductDto save(ProductDto dto) {
        ProductEntity entity = mapToProductEntity.map(dto);
        ProductEntity saveEntity = productRepository.save(entity);
        return mapToProductDto.mapToDto(saveEntity);
    }

    public List<ProductDto> getTechProduct() {
        return productRepository
                .findProductEntitiesByType(ProductType.TECHNIC.toString())
                .stream().map(mapToProductDto::mapToDto)
                .collect(Collectors.toList());
    }

    public List<ProductDto> getFoodProduct() {
        return productRepository
                .findProductEntitiesByType(ProductType.FOOD.toString())
                .stream().map(mapToProductDto::mapToDto)
                .collect(Collectors.toList());
    }

    public List<ProductDto> getAll() {
        return productRepository.findAll().stream()
                .map(mapToProductDto::mapToDto)
                .collect(Collectors.toList());
    }

    public ProductDto getById(Integer id) {
        return mapToProductDto.mapToDto(productRepository.findById(id)
                .orElse(new ProductEntity()));
    }

    public ProductDto getByProductUUID(UUID uuid) {
        return mapToProductDto.mapToDto(productRepository
                .findByProductUUID(uuid)
                .orElse(new ProductEntity()));
    }

    public List<ProductDto> findProductWithPartOfName(String productName) {
        return productRepository.findProductWithPartOfName(productName).stream()
                .map(mapToProductDto::mapToDto)
                .collect(Collectors.toList());
    }

    public void delete(Integer id) {
        productRepository.deleteById(id);
    }
}
