package ru.sber.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.dto.DescriptionFoodProductDto;
import ru.sber.entities.DescriptionFoodProductEntity;
import ru.sber.repositories.DescriptionRepository;
import ru.sber.utils.mapers.to_dto.MapToDescriptionFoodProductDto;
import ru.sber.utils.mapers.to_entity.MapToDescriptionFoodProductEntity;

@Service
@RequiredArgsConstructor
public class DescriptionFoodProductService {

    private final DescriptionRepository descriptionRepository;
    private final MapToDescriptionFoodProductDto mapToDescriptionFoodDto;
    private final MapToDescriptionFoodProductEntity mapToDescriptionFoodEntity;

    public DescriptionFoodProductDto save(DescriptionFoodProductDto dto) {
        DescriptionFoodProductEntity entity = mapToDescriptionFoodEntity.map(dto);
        DescriptionFoodProductEntity saveEntity = descriptionRepository.save(entity);
        return mapToDescriptionFoodDto.mapToDto(saveEntity);
    }

    public DescriptionFoodProductDto getById(Integer id) {
        return mapToDescriptionFoodDto.mapToDto((DescriptionFoodProductEntity) descriptionRepository.findById(id)
                .orElse(new DescriptionFoodProductEntity()));
    }

    public void delete(Integer id) {
        descriptionRepository.deleteById(id);
    }
}
