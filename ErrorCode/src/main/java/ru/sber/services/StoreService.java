package ru.sber.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.dto.StoreDto;
import ru.sber.entities.StoreEntity;
import ru.sber.repositories.StoreRepository;
import ru.sber.utils.mapers.to_dto.MapToStoreDto;
import ru.sber.utils.mapers.to_entity.MapToStoreEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StoreService {

    private final StoreRepository storeRepository;
    private final MapToStoreDto mapToStoreDto;
    private final MapToStoreEntity mapToStoreEntity;

    public StoreDto save(StoreDto dto) {
        StoreEntity storeEntity = mapToStoreEntity.map(dto);
        StoreEntity saveEntity = storeRepository.save(storeEntity);
        return mapToStoreDto.mapToDto(saveEntity);
    }

    public List<StoreDto> getAll() {
        return storeRepository.findAll().stream()
                .map(mapToStoreDto::mapToDto)
                .collect(Collectors.toList());
    }

    public StoreDto getById(Integer id) {
        return mapToStoreDto.mapToDto(storeRepository
                .findById(id).orElse(new StoreEntity()));
    }

    public void delete(Integer id) {
        storeRepository.deleteById(id);
    }
}
