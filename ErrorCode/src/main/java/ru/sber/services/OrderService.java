package ru.sber.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.dto.OrderDto;
import ru.sber.entities.OrderEntity;
import ru.sber.repositories.OrderRepository;
import ru.sber.utils.mapers.to_dto.MapToOrderDto;
import ru.sber.utils.mapers.to_entity.MapToOrderEntity;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final MapToOrderDto mapToOrderDto;
    private final MapToOrderEntity mapToOrderEntity;

    public OrderDto save(OrderDto dto) {
        OrderEntity entity = mapToOrderEntity.map(dto);
        OrderEntity saveEntity = orderRepository.save(entity);
        return mapToOrderDto.mapToDto(saveEntity);
    }

    public List<OrderDto> getAll() {
        return orderRepository.findAll().stream()
                .map(mapToOrderDto::mapToDto)
                .collect(Collectors.toList());
    }

    public List<OrderDto> findBySoldAndPersonUUID(String sold, UUID uuid) {
        return orderRepository.findBySoldAndPersonUUID(sold, uuid).stream()
                .map(mapToOrderDto::mapToDto)
                .collect(Collectors.toList());
    }

    public List<OrderDto> findByPersonUUID(UUID uuid) {
        return orderRepository.findByPersonUUID(uuid).stream()
                .map(mapToOrderDto::mapToDto)
                .collect(Collectors.toList());
    }

    public OrderDto getById(Integer id) {
        return mapToOrderDto.mapToDto(orderRepository.findById(id)
                .orElse(new OrderEntity()));
    }

    public void delete(Integer id) {
        orderRepository.deleteById(id);
    }
}
