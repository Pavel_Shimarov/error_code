package ru.sber.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.dto.DescriptionTechProductDto;
import ru.sber.entities.DescriptionTechProductEntity;
import ru.sber.repositories.DescriptionRepository;
import ru.sber.utils.mapers.to_dto.MapToDescriptionTechDto;
import ru.sber.utils.mapers.to_entity.MapToDescriptionTechProductEntity;

@Service
@RequiredArgsConstructor
public class DescriptionTechProductService {

    private final DescriptionRepository descriptionRepository;
    private final MapToDescriptionTechDto mapToDescriptionTechDto;
    private final MapToDescriptionTechProductEntity mapToDescriptionTechEntity;

    public DescriptionTechProductDto save(DescriptionTechProductDto dto) {
        DescriptionTechProductEntity entity = mapToDescriptionTechEntity.map(dto);
        DescriptionTechProductEntity saveEntity = descriptionRepository.save(entity);
        return mapToDescriptionTechDto.mapToDto(saveEntity);
    }

    public DescriptionTechProductDto getById(Integer id) {
        return mapToDescriptionTechDto.mapToDto((DescriptionTechProductEntity) descriptionRepository.findById(id)
                .orElse(new DescriptionTechProductEntity()));
    }

    public void delete(Integer id) {
        descriptionRepository.deleteById(id);
    }
}
