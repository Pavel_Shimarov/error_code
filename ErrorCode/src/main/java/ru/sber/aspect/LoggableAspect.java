package ru.sber.aspect;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;
import ru.sber.kafka.KafkaConnect;
import ru.sber.loggers.PrinterLogs;

import java.time.LocalDateTime;
import java.util.Arrays;

@Service
@Aspect
@RequiredArgsConstructor
public class LoggableAspect {

    private final PrinterLogs printerLogs;
    private final KafkaConnect kafkaConnect;

    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping) " +
            "|| @annotation(org.springframework.web.bind.annotation.PostMapping) " +
            "|| @annotation(org.springframework.web.bind.annotation.PutMapping)")
    public void loggablePointCutRest() {//можно залогировать таким образом все ресты

    }

    @Pointcut("@annotation(ru.sber.aspect.LoggableAnnotation)")
    public void loggablePointCutInit() {//логирование заполнения таблиц

    }

    @Around("loggablePointCutInit()")
    public Object aroundLoggableInit(ProceedingJoinPoint pjp) throws Throwable {
        return around(pjp);
    }


    @Around("loggablePointCutRest()")
    public Object aroundLoggableRest(ProceedingJoinPoint pjp) throws Throwable {
        return around(pjp);
    }

    private Object around(ProceedingJoinPoint pjp) throws Throwable {
        Object retVal = null;
        String method = cutMethod(pjp.getSignature().toString());

        try {
            String logAfterRunMethod = "\n" + LocalDateTime.now() + ": Класс: "
                    + pjp.getThis().getClass().getName().split("\\$\\$")[0]
                    + "; Метод: " +
                    method +
                    "; Входные параметры: " + Arrays.toString(pjp.getArgs());
            System.out.println(logAfterRunMethod);
            try {
                kafkaConnect.setProducerThread(logAfterRunMethod);
            } catch (Exception e) {
                printerLogs.writeLogByFile(logAfterRunMethod);
            }
            retVal = pjp.proceed();//вызов оригинального метода
            String logBeforeRunMethod = LocalDateTime.now() + ": Класс: " + pjp.getThis().getClass().getName().split("\\$\\$")[0]
                    + "; Метод: " +
                    method +
                    "; Выходные параметры: " + retVal;
            System.out.println(logBeforeRunMethod);
            try {
                kafkaConnect.setProducerThread(logAfterRunMethod);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                printerLogs.writeLogByFile(logBeforeRunMethod);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return retVal;
    }

    private String cutMethod(String str) {
        String reverseNameMethod = new StringBuilder(str).reverse().toString();
        int end = reverseNameMethod.indexOf('.');
        String cutReverseNameMethod = reverseNameMethod.substring(0, end);
        return new StringBuilder(cutReverseNameMethod).reverse().toString();
    }
}
