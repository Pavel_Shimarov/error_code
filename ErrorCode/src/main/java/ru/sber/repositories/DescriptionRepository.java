package ru.sber.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.entities.DescriptionEntity;

@Repository
public interface DescriptionRepository extends JpaRepository<DescriptionEntity, Integer> {

}
