package ru.sber.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sber.entities.ProductEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {

    Optional<ProductEntity> findById(Integer id);

    Optional<ProductEntity> findByProductUUID(UUID productUUID);

    List<ProductEntity> findProductEntitiesByType(String type);

    @Query("SELECT p FROM ProductEntity p WHERE p.productName LIKE CONCAT('%',:productName,'%')")
    List<ProductEntity> findProductWithPartOfName(@Param("productName") String productName);
}
