package ru.sber.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.entities.OrderEntity;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {

    @Query("select o.personUUID from  OrderEntity o where o.id =?1")
    List<OrderEntity> join(Integer id);

    List<OrderEntity> findBySoldAndPersonUUID(String sold, UUID uuid);

    List<OrderEntity> findByPersonUUID(UUID uuid);
}
