package ru.sber.enums;

public enum ProductTechSubType {
    TV,
    PHONE,
    WASHING_MACHINE
}
