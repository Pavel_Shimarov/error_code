package ru.sber.enums;

import lombok.Getter;

@Getter
public enum PersonVariable {

    USER,
    ADMINISTRATOR

}
