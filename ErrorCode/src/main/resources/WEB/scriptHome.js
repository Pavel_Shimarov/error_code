async function fetch502() { // асинхронная функция
    let response = fetch('http://localhost:8080/home/502');//метод для запроса
    let json = await response.json();
    console.log(json);
}

async function fetch404() { // асинхронная функция
    let response = fetch('http://localhost:8080/home/404');//метод для запроса
}

async function fetch302() { // асинхронная функция
    let response = await fetch('http://localhost:8080/home/302');//метод для запроса
}