function handlerDisplayBlock() {
    const element = document.getElementById("about");
    console.log(element);
    element.style.display = element.style.display === 'none' ? 'block' : 'none';
}

async function fetchData() { // асинхронная функция
    let response = await fetch('http://api.github.com/users/PaulMOSQUITO');//метод для запроса
    if (response.ok) {
        let json = await response.json();
        console.log(json);
        let li = document.createElement('li');

        const ul = document.getElementById('tags-id');
        li.innerHTML = json.bio;
        ul.append(li);

        document.getElementById('photo').src = json.avatar_url;
        document.getElementById('name').textContent = json.login;

    } else {
        alert("Ошибка");
    }
}
